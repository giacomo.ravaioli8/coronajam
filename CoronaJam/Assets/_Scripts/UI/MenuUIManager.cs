﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuUIManager : MonoBehaviour
{
    [Header("Second Player")]
    public Animator windowAnimator;
    private int windowHash;

    public GameObject secondPlayerSprite;

    public VsAI useAI;

    public Color playersNormalColor;
    public Color playersSelectedColor;

    public Text onePlayer;
    public Text twoPlayers;

    [Header("Language")]
    public Language lan;

    public Color lanNormalColor;
    public Color lanSelectedColor;

    public Image ita;
    public Image eng;


    // Start is called before the first frame update
    void Start()
    {
        windowHash = Animator.StringToHash("Open");

        useAI.useAIAgent = true;
        onePlayer.color = playersSelectedColor;

        lan.current = Languages.Italian;
        ita.color = lanSelectedColor;
    }
    
    public void ChangeSecondPlayer(bool secondPlayerPresent)
    {
        secondPlayerSprite.SetActive(secondPlayerPresent);

        windowAnimator.SetBool(windowHash, secondPlayerPresent);
        
        useAI.useAIAgent = !secondPlayerPresent;

        twoPlayers.color = secondPlayerPresent ? playersSelectedColor : playersNormalColor;
        onePlayer.color = secondPlayerPresent ? playersNormalColor : playersSelectedColor;
    }

    public void ChangeLanguageVisual (bool itaSelected)
    {
        ita.color = itaSelected ? lanSelectedColor : lanNormalColor;
        eng.color = itaSelected ? lanNormalColor : lanSelectedColor;
    }
}
