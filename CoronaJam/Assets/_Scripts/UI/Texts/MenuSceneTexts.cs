﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuSceneTexts : MonoBehaviour
{
    public Language lan;

    public Text playButton;
    public string[] playButtonTexts = { "Gioca", "Play" };

    public Text quitButton;
    public string[] quitButtonTexts = { "Esci", "Quit" };

    public void UpdateMenuSceneTexts()
    {
        int i = (int)lan.current;

        playButton.text = playButtonTexts[i];
        quitButton.text = quitButtonTexts[i];
    }
}
