﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameSceneTexts : MonoBehaviour
{
    public Text tutorialTitle2;
    private string[] tutorialTitle2Texts = { "e   controlli", "and  controls" };

    public Text winCondition;
    private string[] winConditionTexts = { "Vince chi ha influenzato\ncon un sacco di fake news\npiù        allo scadere\ndel tempo!",
                                          "Wins whoever influenced\nwith lots of fake news more\n    when the\ntime runs out!" };

    public Text money;
    private string[] moneyTexts = { "Ogni giocatore guadagna 5€\n per ogni persona influenzata", "Each player earns 5£\n for each influenced person" };

    public Text moneyTip;
    private string[] moneyTipTexts = { "Puoi usare i soldi per influenzare\nancora più persone!", "You can use your money to influence\n more people!" };

    public Text distractions;
    private string[] distractionsTexts = { "Quando passano i camioncini, le persone\nvengono distratte e ne perdi l'influenza!", "Whenever a truck passes, people are distracted\n and you lose your influence over them!" };

    public Text player1;
    private string[] player1Texts = { "Giocatore 1", "Player 1" };

    public Text player2;
    private string[] player2Texts = { "Giocatore 2", "Player 2" };

    public Text arrows;
    private string[] arrowsTexts = { "Frecce", "Arrows" };

    public Text tutorialNextButton;
    private string[] tutorialNextButtonTexts = { "Avanti", "Next" };

    public Text tutorialBackButton;
    public Text devicesBackButton;
    private string[] tutorialBackButtonTexts = { "Indietro", "Back" };

    public Text devicesTitle;
    private string[] devicesTitleTexts = { "Dispositivi", "Devices" };

    public Text influencedPerson;
    private string[] influencedPersonTexts = { "Puoi comprare smartphone e radio\nper influenzare le persone\nnella tua mappa",
                                              "You can buy phones and radios\nto influence people\non your map" };

    public Text smartphone;
    private string[] smartphoneTexts = { "Smartphone", "Phone" };

    public Text curePeople;
    private string[] curePeopleTexts = { "Puoi comprare i giornali o chiamare il premier\nper curare le persone nella mappa avversaria",
                                        "You can buy newspapers or call the \"Premier\"\nto cure people on the opponent's map" };

    public Text journal;
    private string[] journalTexts = { "Giornale", "Newspaper" };

    public Text journalTip;
    private string[] journalTipTexts = { "Quando usi il giornale il\npuntatore si sposta nella\nmappa avversaria", "When you use the newspaper the\ncursor will move to the\nopponent's map" };

    public Text premierTip;
    private string[] premierTipTexts = { "Dimezza il numero\ndi influenzati\ndell'avversario", "Halves the number\nof opponent\ninfluenced people " };

    public Text playButton;
    private string[] playButtonTexts = { "Gioca", "Play" };

    public Text pauseTitle;
    private string[] pauseTitleTexts = { "Pausa", "Pause" };

    public Text pauseHint;
    private string[] pauseHintTexts = { "Per la\ntua pausa\npremi\n(ESC)", "For a\nbreak\npress\n(ESC)" };

    public Text pauseBackButton;
    private string[] pauseBackButtonTexts = { "Riprendi", "Resume" };

    public Text pauseQuitButton;
    private string[] pauseQuitButtonTexts = { "Esci", "Quit" };
    
    public Text replayButton;
    private string[] replayButtonTexts = { "Rigioca", "Play Again" };

    public Text menuButton;
    private string[] menuButtonTexts = { "Menu", "Menu" };

    private string[] currencies = { "€", "£" };

    public void SetGameSceneTexts()
    {
        int i = (int)GameManager.instance.lan.current;

        tutorialTitle2.text = tutorialTitle2Texts[i];
        winCondition.text = winConditionTexts[i];
        money.text = moneyTexts[i];
        moneyTip.text = moneyTipTexts[i];
        distractions.text = distractionsTexts[i];
        player1.text = player1Texts[i];
        player2.text = player2Texts[i];
        arrows.text = arrowsTexts[i];
        tutorialNextButton.text = tutorialNextButtonTexts[i];
        tutorialBackButton.text = tutorialBackButtonTexts[i];
        devicesBackButton.text = tutorialBackButtonTexts[i];
        devicesTitle.text = devicesTitleTexts[i];
        influencedPerson.text = influencedPersonTexts[i];
        smartphone.text = smartphoneTexts[i];
        curePeople.text = curePeopleTexts[i];
        journal.text = journalTexts[i];
        journalTip.text = journalTipTexts[i];
        premierTip.text = premierTipTexts[i];
        playButton.text = playButtonTexts[i];
        pauseTitle.text = pauseTitleTexts[i];
        pauseHint.text = pauseHintTexts[i];
        pauseBackButton.text = pauseBackButtonTexts[i];
        pauseQuitButton.text = pauseQuitButtonTexts[i];
        replayButton.text = replayButtonTexts[i];
        menuButton.text = menuButtonTexts[i];
        money.text = moneyTexts[i];
    }

    public string GetCurrency(int i)
    {
        return currencies[i];
    }
}
