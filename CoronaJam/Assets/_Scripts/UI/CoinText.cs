﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinText : MonoBehaviour
{
    public Text coins;
        
    public void UpdateCoins1()
    {
        coins.text = "Coins: " + GameManager.instance.player1.GetComponent<PlayerStatus>().money;
    }

    public void UpdateCoins2()
    {
        coins.text = "Coins: " + GameManager.instance.player2.GetComponent<PlayerStatus>().money;
    }
}
