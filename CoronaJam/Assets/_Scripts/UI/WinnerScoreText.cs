﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinnerScoreText : MonoBehaviour
{
    private GameObject player1;
    private GameObject player2;

    [SerializeField]
    private GameObject winner;
    [SerializeField]
    private Text winnerName;
    [SerializeField]
    private Text winnerInflu;

    [SerializeField]
    private GameObject runnerUp;
    [SerializeField]
    private Text runnerUpName;
    [SerializeField]
    private Text runnerUpInflu;

    [SerializeField]
    private GameObject draft;

    void Start()
    {
        player1 = GameManager.instance.player1;
        player2 = GameManager.instance.player2;
    }

    public void UpdateText()
    {
        int winnerId = 0;

        if (player1.GetComponent<PlayerStatus>().numberOfInfluenced > player2.GetComponent<PlayerStatus>().numberOfInfluenced)
            winnerId = 1;
        else if (player2.GetComponent<PlayerStatus>().numberOfInfluenced > player1.GetComponent<PlayerStatus>().numberOfInfluenced)
            winnerId = 2;

        if (winnerId == 0)
        {
            winner.SetActive(false);
            runnerUp.SetActive(false);
            draft.SetActive(true);
        }
        else
        {
            winnerName.text = winnerId == 1 ? player1.GetComponent<PlayerStatus>().playerName : player2.GetComponent<PlayerStatus>().playerName;
            winnerInflu.text = winnerId == 1 ? player1.GetComponent<PlayerStatus>().numberOfInfluenced.ToString() : player2.GetComponent<PlayerStatus>().numberOfInfluenced.ToString();

            runnerUpName.text = winnerId == 1 ? player2.GetComponent<PlayerStatus>().playerName : player1.GetComponent<PlayerStatus>().playerName;
            runnerUpInflu.text = winnerId == 1 ? player2.GetComponent<PlayerStatus>().numberOfInfluenced.ToString() : player1.GetComponent<PlayerStatus>().numberOfInfluenced.ToString();
        }
    }
}
