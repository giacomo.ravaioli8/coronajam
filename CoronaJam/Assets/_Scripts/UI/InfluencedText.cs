﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfluencedText : MonoBehaviour
{
    public Text numberOfInfluenced;
    
    public void UpdateNumberOfInfluenced1()
    {
        numberOfInfluenced.text = "Influenced: " + GameManager.instance.player1.GetComponent<PlayerStatus>().numberOfInfluenced;
    }

    public void UpdateNumberOfInfluenced2()
    {
        numberOfInfluenced.text = "Influenced: " + GameManager.instance.player2.GetComponent<PlayerStatus>().numberOfInfluenced;
    }
}
