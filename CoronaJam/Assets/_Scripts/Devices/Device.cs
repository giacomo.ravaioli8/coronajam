﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum DeviceType { Influencer, Healer }

[CreateAssetMenu]
public class Device : ScriptableObject
{
    public string deviceName;

    public DeviceType type;
    public int cost;
    
    public int duration;

    public GameObject prefab;
}
