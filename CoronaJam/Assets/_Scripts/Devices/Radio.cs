﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Radio : MonoBehaviour
{
    public Device device;
    public Color colorP1;
    public Color colorP2;
    
    [SerializeField]
    private SpriteRenderer wavesRenderer;

    private float timer = 0f;

    private void Update()
    {
        if (GameManager.instance.paused)
            return;

        timer += Time.deltaTime;

        if (timer >= device.duration)
            Destroy(gameObject);
    }

    public void SetWavesColor(int playerId)
    {
        wavesRenderer.color = playerId == 1 ? colorP1 : colorP2;
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        collider.gameObject.GetComponent<Person>()?.Influence();
    }
}
