﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Journal : MonoBehaviour
{
    public Device device;

    public GameObject player;

    public LayerMask targetLayer;

    public Color placedColor;
    public Color placingColor;

    private bool placed = false;
    private bool triggered = false;

    private void Awake()
    {
        GetComponent<SpriteRenderer>().color = placingColor;
    }

    private void Update()
    {
        if (!placed)
        {
            transform.position = player.transform.position;
        }
    }

    public void Place()
    {
        placed = true;
        GetComponent<SpriteRenderer>().color = placedColor;
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (!placed || triggered)
            return;

        triggered = true;

        collider.gameObject.GetComponent<Person>().Cure();

        Destroy(gameObject, 0.5f);
    }
}
