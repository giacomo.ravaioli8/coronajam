﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Smartphone : MonoBehaviour
{
    public Device device;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.Equals(transform.parent.gameObject))
            return;

        collision.gameObject.GetComponent<Person>()?.Influence();
    }
}
