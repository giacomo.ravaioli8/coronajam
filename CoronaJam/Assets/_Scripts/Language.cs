﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Languages { Italian, English }

[CreateAssetMenu]
public class Language : ScriptableObject
{
    public Languages current = Languages.Italian;

    public void SetLanguage(int i)
    {
        current = (Languages)i;
    }

    public void SetLanguage(Languages newLan)
    {
        current = newLan;
    }
}
