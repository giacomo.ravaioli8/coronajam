﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimerScript : MonoBehaviour
{
    public Text gameTimerText;
    [SerializeField]
    private float gameTimer = 0f;

    // Start is called before the first frame update
    void Start()
    {
        gameTimer = GameManager.instance.gameDuration;
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.instance.paused || GameManager.instance.ended)
            return;

        gameTimer -= Time.deltaTime;

        if (gameTimer <= 0)
            GameManager.instance.EndGame();

        int seconds = (int)(gameTimer % 60);
        int minutes = (int)(gameTimer / 60) % 60;
        string timerString = string.Format("{0:0}:{1:00}", minutes,seconds);
        gameTimerText.text = timerString;
    }
}
