﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions : MonoBehaviour
{
    private PlayerStatus status;

    public LayerMask targetLayer;

    [Header("Devices")]
    public Device journal;
    private GameObject placingJournal;

    public Device radio;

    public Device smartphone;
    public GameObject currentUnderneath;
    private bool currentUnderneathOut = false;

    public Device conte;
    [Range(0, 1)]
    public float conteCurePercentage = 0f;

    private void Awake()
    {
        status = GetComponent<PlayerStatus>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (currentUnderneathOut)
        {
            currentUnderneathOut = false;
            currentUnderneath = null;
        }
    }

    public bool UseSmartphone()
    {
        if(status.money >= smartphone.cost && currentUnderneath != null)
        {
            status.ReduceMoney(smartphone.cost);
            currentUnderneath.GetComponent<Person>().AssignSmartphone(true);
            if(GetComponent<PlayerStatus>().id == 2) Debug.Log("... used");
            return true;
        }
        else
        {
            if(status.money < smartphone.cost)
                Debug.Log(GetComponent<PlayerStatus>() + " | Not enough money");
            
            if(currentUnderneath == null)
                Debug.Log(GetComponent<PlayerStatus>() + " | No current underneath");

            return false;
        }
    }

    public bool UseRadio()
    {
        if (status.money >= radio.cost)
        {
            status.ReduceMoney(radio.cost);
            GameObject obj = Instantiate(radio.prefab, transform.position, Quaternion.identity);
            obj.GetComponent<Radio>().SetWavesColor(status.id);
            if (GetComponent<PlayerStatus>().id == 2) Debug.Log("... used");
            return true;
        }
        else
        {
            Debug.Log(GetComponent<PlayerStatus>() + " | Not enough money");
            return false;
        }
    }

    public bool UseJournal()
    {
        Debug.Log("Placing? " + status.placingJournal);
        if (status.placingJournal)
        {
            placingJournal.GetComponent<Journal>().Place();

            transform.position = new Vector3 (-transform.position.x, transform.position.y, 0f);
            status.placingJournal = false;

            if (GetComponent<PlayerStatus>().id == 2) Debug.Log("... used");

            return true;
        }
        else
        {
            if (status.money >= journal.cost)
            {
                status.ReduceMoney(journal.cost);
                placingJournal = Instantiate(journal.prefab, transform.position, Quaternion.identity);
                placingJournal.GetComponent<Journal>().player = gameObject;

                status.placingJournal = true;

                transform.position = new Vector3(-transform.position.x, transform.position.y, 0f); ;
                
                return true;
            }
            else
            {
                Debug.Log(GetComponent<PlayerStatus>() + " | Not enough money");
                return false;
            }
        }
    }

    public bool UseConte()
    {
        if (status.money >= conte.cost)
        {
            status.ReduceMoney(conte.cost);
            StartCoroutine(IConte());
            if (GetComponent<PlayerStatus>().id == 2) Debug.Log("... used");
            return true;
        }
        else
        {
            Debug.Log(GetComponent<PlayerStatus>() + " | Not enough money");
            return false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (currentUnderneath == null)
            currentUnderneath = collision.gameObject;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (currentUnderneath != null && collision.gameObject.Equals(currentUnderneath))
            currentUnderneathOut = true;
    }

    private IEnumerator IConte()
    {
        status.SetConteInAction(true);
        UIManager.instance.ShowContePanel(status.id, true);

        GameObject people = status.id == 1 ? GameManager.instance.people2 : GameManager.instance.people1;
        int toCure = 0;
        int toRemove = 0;

        for (int i=0; i < people.transform.childCount; i++)
        {
            if (people.transform.GetChild(i).GetComponent<Person>().IsInfluenced())
            {
                toCure++;
            }
        }

        toCure = Mathf.FloorToInt(toCure * conteCurePercentage);
        toRemove = Mathf.FloorToInt(people.GetComponent<PlayerStatus>().numberOfInfluenced * conteCurePercentage);

        Debug.Log("People influenced: " + people.transform.childCount + " | TO CURE = " + toCure + " | TO REMOVE = " + toRemove);

        for (int i = 0; i < people.transform.childCount && toCure > 0; i++)
        {
            if (people.transform.GetChild(i).GetComponent<Person>().IsInfluenced())
            {
                people.transform.GetChild(i).GetComponent<Person>()?.Cure();
                toCure--;
            }
        }

        people.GetComponent<PlayerStatus>().numberOfInfluenced -= toRemove;

        yield return new WaitForSeconds(conte.duration);

        status.SetConteInAction(false);
        UIManager.instance.ShowContePanel(status.id, false);
    }
}
