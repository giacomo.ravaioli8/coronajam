﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatus : MonoBehaviour
{
    public int id;
    public int numberOfInfluenced = 0;
    public string playerName;

    public float money;
    public GameEvent _UpdateCoins1;
    public GameEvent _UpdateCoins2;

    public bool placingJournal = false;
    public bool conteInAction = false;
    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(MoneyUpdate());
    }

    public IEnumerator MoneyUpdate()
    {
        while (true)
        {
            //updatemoney
            money += numberOfInfluenced * 5;
            UIManager.instance.UpdateCoins(id);

            if (id == 1)
                _UpdateCoins1.Raise();
            else
                _UpdateCoins2.Raise();

            yield return new WaitForSeconds(GameManager.instance.moneyUpdateTime);
        }
    }

    public void ReduceMoney (int amount)
    {
        money -= amount;
        UIManager.instance.UpdateCoins(id);
    }

    public void IncrementNumberInfluenced()
    {
        numberOfInfluenced++;
        UIManager.instance.UpdateNumberOfInfluenced(id);
        UIManager.instance.UpdateRates(id, numberOfInfluenced * 5);
    }

    public void DecrementNumberInfluenced()
    {
        numberOfInfluenced--;
        UIManager.instance.UpdateNumberOfInfluenced(id);
        UIManager.instance.UpdateRates(id, numberOfInfluenced * 5);
    }

    public void SetConteInAction(bool inAction)
    {
        conteInAction = inAction;
    }
}
