﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    public float cursorMovementSpeed = 5;

    [SerializeField]
    private GameEvent smartPhone_1;
    [SerializeField]
    private GameEvent smartPhone_2;
    [SerializeField]
    private GameEvent radio_1;
    [SerializeField]
    private GameEvent radio_2;
    [SerializeField]
    private GameEvent journal_1;
    [SerializeField]
    private GameEvent journal_2;
    [SerializeField]
    private GameEvent _ConteSexyPremier;

    public BoxCollider2D player1Boundary;
    public BoxCollider2D player2Boundary;

    private PlayerStatus status;
    private PlayerActions actions;

    private void Awake()
    {
        status = GetComponent<PlayerStatus>();
        actions = GetComponent<PlayerActions>();
    }

    void Update()
    {
        if (GameManager.instance.paused)
            return;

        if (status.id == 1 && !GameManager.instance.Conte2InAction())
        {

            float horOff = transform.position.x + Input.GetAxis("Horizontal") * Time.deltaTime * cursorMovementSpeed;
            float verOff = transform.position.y + Input.GetAxis("Vertical") * Time.deltaTime * cursorMovementSpeed;

            BoxCollider2D toUse = status.placingJournal ? player2Boundary : player1Boundary;

            horOff = Mathf.Clamp(horOff, toUse.transform.position.x - toUse.bounds.extents.x, toUse.transform.position.x + toUse.bounds.extents.x);
            verOff = Mathf.Clamp(verOff, toUse.transform.position.y - toUse.bounds.extents.y, toUse.transform.position.y + toUse.bounds.extents.y);

            transform.position = new Vector3(horOff, verOff, 0f);

            if (Input.GetKeyDown(KeyCode.Q) && !status.placingJournal)
            {
                if (actions.UseSmartphone())
                    smartPhone_1.Raise();
            }
            if (Input.GetKeyDown(KeyCode.E) && !status.placingJournal)
            {
                if (actions.UseRadio())
                    radio_1.Raise();
            }
            if (Input.GetKeyDown(KeyCode.R) && !status.conteInAction)
            {
                if (actions.UseJournal())
                    journal_1.Raise();
            }
            if (Input.GetKeyDown(KeyCode.F) && !status.conteInAction)
            {
                if (actions.UseConte())
                    _ConteSexyPremier.Raise();
            }
        }
        else if (!GameManager.instance.usingAI && status.id == 2 && !GameManager.instance.Conte1InAction())
        {
            float horOff = transform.position.x + Input.GetAxis("Horizontal2") * Time.deltaTime * cursorMovementSpeed;
            float verOff = transform.position.y + Input.GetAxis("Vertical2") * Time.deltaTime * cursorMovementSpeed;

            BoxCollider2D toUse = status.placingJournal ? player1Boundary : player2Boundary;

            horOff = Mathf.Clamp(horOff, toUse.transform.position.x - toUse.bounds.extents.x, toUse.transform.position.x + toUse.bounds.extents.x);
            verOff = Mathf.Clamp(verOff, toUse.transform.position.y - toUse.bounds.extents.y, toUse.transform.position.y + toUse.bounds.extents.y);

            transform.position = new Vector3(horOff, verOff, 0f);

            if (Input.GetKeyDown(KeyCode.B) && !status.placingJournal)
            {
                if (actions.UseSmartphone())
                    smartPhone_2.Raise();
            }
            if (Input.GetKeyDown(KeyCode.N) && !status.placingJournal)
            {
                if (actions.UseRadio())
                    radio_2.Raise();
            }
            if (Input.GetKeyDown(KeyCode.M) && !status.conteInAction)
            {
                if (actions.UseJournal())
                    journal_2.Raise();
            }
            if (Input.GetKeyDown(KeyCode.L) && !status.conteInAction)
            {
                if (actions.UseConte())
                    _ConteSexyPremier.Raise();
            }
        }
    }
}
