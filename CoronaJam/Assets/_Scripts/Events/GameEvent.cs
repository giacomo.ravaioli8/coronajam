﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(GameEvent))]
public class GameEventEditor : Editor
{
    private GameEvent _data;

    private void OnEnable()
    {
        _data = target as GameEvent;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (GUILayout.Button("Raise"))
        {
            _data.Raise();
        }
    }
}
#endif

[CreateAssetMenu]
public class GameEvent : ScriptableObject
{
    private List<GameEventListener> _listeners = new List<GameEventListener>();

    public void Raise()
    {
        for (int i = 0; i < _listeners.Count; i++)
        {
            _listeners[i].OnEventRaised();
        }
    }

    public void Register(GameEventListener listener)
    {
        if (_listeners.Contains(listener))
        {
            Debug.LogError("frick");
            return;
        }

        _listeners.Add(listener);
    }

    public void Unregister(GameEventListener listener)
    {
        if (!_listeners.Contains(listener))
        {
            Debug.LogError("double frick");
            return;
        }

        _listeners.Remove(listener);
    }
}
