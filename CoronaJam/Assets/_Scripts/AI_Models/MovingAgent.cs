﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingAgent : MonoBehaviour
{
    public Animator anim;
    protected int idX = 0;
    protected int idY = 0;

    public float speed = 0.3f;

    public float stopThreshold = 0.1f;

    public Node currentNode;
    public Node nextNode;

    [Header("Pathfinding")]
    protected MapGrid mapGrid;
    protected Edge[] edgesToOut;
    
    private void Awake()
    {
        anim = GetComponent<Animator>();

        if (anim == null)
            anim = GetComponentInChildren<Animator>();

        idX = Animator.StringToHash("X");
        idY = Animator.StringToHash("Y");
    }

    public virtual void AgentSetup(MapGrid g)
    {
        mapGrid = g;
        currentNode = mapGrid.GetRandomOutNode(null);

        transform.position = currentNode.transform.position;
    }

    protected void ExitMap()
    {
        edgesToOut = mapGrid.FindPathToOut(currentNode);

        /*foreach (Edge e in edgesToOut)
        {
            Debug.Log(mapGrid.name + " | Going from " + e.from.name + " to " + e.to.name);
        }*/
    }

    protected IEnumerator IMoveToExit()
    {
        int i = 0;

        nextNode = edgesToOut[0].to;
        UpdateAnimator();

        while (true)
        {
            if (Mathf.Abs((transform.position - nextNode.transform.position).magnitude) < stopThreshold)
            {
                i++;
                if (i >= edgesToOut.Length)
                {
                    Destroy(gameObject);

                    yield break;
                }

                currentNode = nextNode;
                nextNode = edgesToOut[i].to;

                if (anim != null)
                    UpdateAnimator();
            }
            else
            {
                if (!GameManager.instance.paused && !GameManager.instance.ended)
                    transform.position = Vector3.MoveTowards(transform.position, nextNode.transform.position, Time.deltaTime * speed);

                yield return null;
            }
        }
    }

    public void UpdateAnimator()
    {
        Vector3 dir = (nextNode.transform.position - currentNode.transform.position).normalized;
        if (Mathf.Abs(dir.x) > Mathf.Abs(dir.y))
        {
            anim.SetFloat(idX, dir.x > 0 ? 1 : -1);
            anim.SetFloat(idY, 0);
        }
        else if (Mathf.Abs(dir.x) < Mathf.Abs(dir.y))
        {
            anim.SetFloat(idX, 0);
            anim.SetFloat(idY, dir.y > 0 ? 1 : -1);
        }
    }
}