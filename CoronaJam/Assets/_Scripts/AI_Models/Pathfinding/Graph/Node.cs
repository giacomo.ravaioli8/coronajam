﻿using UnityEngine;
using System.Collections.Generic;

public class Node : MonoBehaviour {
    
	public GameObject sceneObject;
    public List<Node> connectedNodes;

    private void Start()
    {
        sceneObject = gameObject;
    }
}