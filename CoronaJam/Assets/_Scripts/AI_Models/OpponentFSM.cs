﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CRBT;

public enum FSM_STATES { Aggressive, Conservative, Acquiring }

[RequireComponent(typeof(OpponentActions))]
public class OpponentFSM : MonoBehaviour
{
    public FSM f;

    public FSM_STATES currentState = FSM_STATES.Acquiring;

    private BehaviorTree aggr_bt;
    private BehaviorTree cons_bt;
    private BehaviorTree acqu_bt;

    private OpponentActions oppActions;

    public bool high_adv_toggle = false;
    public bool adv_toggle = false;
    public bool disadv_toggle = false;

    public int influLimit = 1;
    public int influVariance = 1;
    public BTDecoratorLimit influDecLimit;
    public int cureLimit = 3;
    public int cureVariance = 1;
    public BTDecoratorLimit cureDecLimit;

    public bool currentActionCompleted = true;

    private void Awake()
    {
        oppActions = GetComponent<OpponentActions>();
    }

    public void SetOpponentFSM()
    {
        //In disadvantage
        SetAggressiveTree();
        //In high advantage
        SetConservativeTree();
        //In advantage
        SetAcquiringTree();

        bool startActionCompleted = false;

        FSMState start = new FSMState("Start");

        FSMState aggressive = new FSMState("Aggressive");
        FSMState conservative = new FSMState("Conservative");
        FSMState acquiring = new FSMState("Acquiring");

        FSMCondition high_adv = new FSMCondition(() => { return CheckAdvantage() == 1; });
        FSMCondition adv = new FSMCondition(() => { return CheckAdvantage() == 0; });
        FSMCondition disadv = new FSMCondition(() => { return CheckAdvantage() == -1; });

        start.AddTransition(new FSMTransition(() => { return startActionCompleted; }), acquiring);
        start.stayActions.Add(() => { oppActions.UseSmartphone(); startActionCompleted = true; });

        aggressive.AddTransition(new FSMTransition(high_adv), conservative);
        aggressive.AddTransition(new FSMTransition(adv), acquiring);
        aggressive.enterActions.Add(() => { currentState = FSM_STATES.Aggressive; });
        aggressive.stayActions.Add(() => { aggressive.ToString(); UpdateBehaviour(FSM_STATES.Aggressive); });

        conservative.AddTransition(new FSMTransition(disadv), aggressive);
        conservative.AddTransition(new FSMTransition(adv), acquiring);
        conservative.enterActions.Add(() => { currentState = FSM_STATES.Conservative; });
        conservative.stayActions.Add(() => { conservative.ToString(); UpdateBehaviour(FSM_STATES.Conservative); });

        acquiring.AddTransition(new FSMTransition(high_adv), conservative);
        acquiring.AddTransition(new FSMTransition(disadv), aggressive);
        acquiring.enterActions.Add(() => { currentState = FSM_STATES.Acquiring; });
        acquiring.stayActions.Add(() => { acquiring.ToString(); UpdateBehaviour(FSM_STATES.Acquiring); });

        f = new FSM(start);
    }

    //returns -1 for disadvantage, 0 advantage, 1 high adv
    private int CheckAdvantage()
    {
        int d = GetComponent<PlayerStatus>().numberOfInfluenced - GameManager.instance.player1.GetComponent<PlayerStatus>().numberOfInfluenced;

        if (d < 0)
            return -1;
        else if (d > 10)
            return 1;
        else
            return 0;
    }
    
    //States behaviors
    private void SetAggressiveTree()
    {
        BTAction smartphone = new BTAction(oppActions.UseSmartphone, "Smartphone");
        BTAction radio = new BTAction(oppActions.UseRadio, "Radio");
        
        BTRandomSelector influRand = new BTRandomSelector(new BTAction[] { smartphone, radio }, "Influ");
        influDecLimit = new BTDecoratorLimit(1, influRand);
        BTDecoratorUntilFail influFail = new BTDecoratorUntilFail(influDecLimit);
        

        BTAction journal = new BTAction(oppActions.UseJournal, "Journal");
        BTAction conte = new BTAction(oppActions.UseConte, "Conte");

        BTRandomSelector cureRand = new BTRandomSelector(new BTAction[] { journal, conte }, "Cure");
        cureDecLimit = new BTDecoratorLimit(1, cureRand);
        BTDecoratorUntilFail cureFail = new BTDecoratorUntilFail(cureDecLimit);
        
        BTSequence actions = new BTSequence(new IBTTask[] { influFail, cureFail}, "actions_seq");

        BTAction wait = new BTAction(oppActions.WaitForMoney, "wait money");

        BTSelector root = new BTSelector(new IBTTask[] { actions, wait }, "aggr_root");

        aggr_bt = new BehaviorTree(root);
    }

    private void SetConservativeTree()
    {
        BTAction smartphone = new BTAction(oppActions.UseSmartphone, "Smartphone");
        BTAction radio = new BTAction(oppActions.UseRadio, "Radio");
        BTAction journal = new BTAction(oppActions.UseJournal, "Journal");
        BTAction conte = new BTAction(oppActions.UseConte, "Conte");

        BTRandomSelector rand = new BTRandomSelector(new IBTTask[] { smartphone, radio, journal, conte }, "randSel");
        BTAction wait = new BTAction(oppActions.WaitForMoney, "wait money");

        BTRandomSelector root = new BTRandomSelector(new IBTTask[] { wait, rand }, "cons_root");

        cons_bt = new BehaviorTree(root);
    }

    private void SetAcquiringTree()
    {
        BTAction smartphone = new BTAction(oppActions.UseSmartphone, "Smartphone");
        BTAction radio = new BTAction(oppActions.UseRadio, "Radio");
        BTRandomSelector rand = new BTRandomSelector(new IBTTask[] { smartphone, radio }, "randSel");

        BTAction wait = new BTAction(oppActions.WaitForMoney, "wait money");

        BTSelector root = new BTSelector(new IBTTask[] { rand, wait }, "acqu_root");

        acqu_bt = new BehaviorTree(root);
    }

    private void UpdateBehaviour(FSM_STATES state)
    {
        Debug.Log(currentState + " | In action? " + (!currentActionCompleted).ToString().ToUpper());

        if (!currentActionCompleted)
            return;
            
        bool result = true;

        Debug.Log("Limits: INFLU " + influDecLimit.maxRepetitions + " | CURE " + cureDecLimit.maxRepetitions);

        //while (result)
        //{
            switch (state)
            {
                case FSM_STATES.Aggressive:
                    result = aggr_bt.Step();
                    if (!result)
                    {
                        influDecLimit.count = 0;
                        influDecLimit.maxRepetitions = Random.Range(0, influLimit + influVariance);
                        cureDecLimit.count = 0;
                        cureDecLimit.maxRepetitions = Random.Range(cureLimit - cureVariance, cureLimit + cureVariance);
                        Debug.Log("Limits: INFLU " + influDecLimit.maxRepetitions + " | CURE " + cureDecLimit.maxRepetitions);
                    }
                    break;
                case FSM_STATES.Conservative:
                    result = cons_bt.Step();
                    break;
                case FSM_STATES.Acquiring:
                    result = acqu_bt.Step();
                    break;
            }
        //}
    }
}
