﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CRBT;

public class OpponentActions : MonoBehaviour
{
    public Device smartphone;
    public Device radio;
    public Device journal;
    public Device conte;

    public bool smartphoneUsable = true;
    public bool radioUsable = true;
    public bool journalUsable = true;
    public bool conteUsable = true;

    private OpponentFSM fsm;
    private PlayerStatus ps;

    private void Awake()
    {
        fsm = GetComponent<OpponentFSM>();
        ps = GetComponent<PlayerStatus>();
    }

    //Opponent Actions
    public bool WaitForMoney()
    {
        fsm.currentActionCompleted = false;

        StartCoroutine(IMoveRandom());

        return true;
    }

    public bool UseSmartphone()
    {
        if (!smartphoneUsable)
            return true;

        if (smartphone.cost > ps.money || !GetComponent<OpponentFSM>().currentActionCompleted)
            return false;

        Debug.Log("Smartphone...");
        fsm.currentActionCompleted = false;

        StartCoroutine(IUseSmartphone());
        
        return true;
    }

    public bool UseRadio()
    {
        if (!radioUsable)
            return true;

        if (radio.cost > ps.money || !GetComponent<OpponentFSM>().currentActionCompleted)
            return false;

        Debug.Log("Radio...");
        fsm.currentActionCompleted = false;
        
        StartCoroutine(IUseRadio());

        return true;
    }

    public bool UseJournal()
    {
        if (!journalUsable)
            return true;

        if (journal.cost > ps.money || !GetComponent<OpponentFSM>().currentActionCompleted)
            return false;

        Debug.Log("Journal...");
        fsm.currentActionCompleted = false;

        StartCoroutine(IUseJournal());

        return true;
    }

    public bool UseConte()
    {
        if (!conteUsable)
            return true;

        if (conte.cost > ps.money || !GetComponent<OpponentFSM>().currentActionCompleted)
            return false;

        Debug.Log("Conte...");
        fsm.currentActionCompleted = false;

        GetComponent<PlayerActions>().UseConte();

        fsm.currentActionCompleted = true;

        return true;
    }

    private IEnumerator IUseSmartphone()
    {
        bool targetReached = false;

        GameObject target = FindAndLockTargetPerson();
        
        //If there isn't at least one person not influenced, stop
        if (target == null)
            yield break;

        while (!targetReached)
        {
            if (target == null)
                target = FindAndLockTargetPerson();

            transform.position += (target.transform.position - transform.position).normalized * Time.deltaTime * GetComponent<InputController>().cursorMovementSpeed;
            
            yield return null;

            if (GetComponent<PlayerActions>().currentUnderneath != null && GetComponent<PlayerActions>().currentUnderneath.Equals(target))
            {
                targetReached = true;
                GetComponent<PlayerActions>().UseSmartphone();
                target.GetComponent<PersonMovement>().leaveActive = true;
            }   
        }

        fsm.currentActionCompleted = true;
    }

    private IEnumerator IUseRadio()
    {
        bool targetReached = false;
        
        Vector3 targetPosition = FindRandomPosition(GetComponent<InputController>().player2Boundary.bounds.extents.x, GetComponent<InputController>().player2Boundary.bounds.center);
        
        while (!targetReached)
        {
            transform.position += (targetPosition - transform.position).normalized * Time.deltaTime * GetComponent<InputController>().cursorMovementSpeed;

            yield return null;

            if (Mathf.Abs((targetPosition - transform.position).magnitude) < 0.02f)
            {
                targetReached = true;
                GetComponent<PlayerActions>().UseRadio();
            }
        }

        fsm.currentActionCompleted = true;
    }

    private IEnumerator IUseJournal()
    {
        bool targetReached = false;

        Vector3 targetPosition = FindRandomPosition(GetComponent<InputController>().player1Boundary.bounds.extents.x, GetComponent<InputController>().player1Boundary.bounds.center);

        GetComponent<PlayerActions>().UseJournal();

        while (!targetReached)
        {
            //Debug.Log("Journal dir: " + (targetPosition - transform.position));
            transform.position += (targetPosition - transform.position).normalized * Time.deltaTime * GetComponent<InputController>().cursorMovementSpeed;

            yield return null;

            //Debug.Log("Journal dist: " + (targetPosition - transform.position).magnitude);

            if ((targetPosition - transform.position).magnitude <= 0.1f)
            {
                targetReached = true;
                GetComponent<PlayerActions>().UseJournal();
            }
        }

        fsm.currentActionCompleted = true;
    }

    private IEnumerator IMoveRandom()
    {
        bool targetReached = false;

        Vector3 targetPosition = FindRandomPosition(GetComponent<InputController>().player2Boundary.bounds.extents.x, GetComponent<InputController>().player2Boundary.bounds.center);

        while (!targetReached)
        {
            transform.position += (targetPosition - transform.position).normalized * Time.deltaTime * GetComponent<InputController>().cursorMovementSpeed;

            yield return null;

            if (Mathf.Abs((targetPosition - transform.position).magnitude) < 0.02f)
            {
                targetReached = true;
            }
        }

        fsm.currentActionCompleted = true;
    }

    private Vector3 FindRandomPosition(float boundExtentX, Vector3 boundCenter)
    {
        Debug.Log("Collider center: " + boundCenter + " | extent: " + boundExtentX);

        Vector2 randomPoint = Random.insideUnitCircle * boundExtentX * 0.8f;
        Vector3 targetPosition = boundCenter + new Vector3(randomPoint.x, randomPoint.y);

        Debug.DrawRay(GetComponent<InputController>().player2Boundary.bounds.center, new Vector3(randomPoint.x, randomPoint.y) - GetComponent<InputController>().player2Boundary.bounds.center, Color.red, 6f);

        return targetPosition;
    }

    private GameObject FindAndLockTargetPerson()
    {
        GameObject target = null;

        for (int i = 0; i < GameManager.instance.people2.transform.childCount; i++)
        {
            if (!GameManager.instance.people2.transform.GetChild(i).GetComponent<Person>().IsInfluenced())
            {
                target = GameManager.instance.people2.transform.GetChild(i).gameObject;
                target.GetComponent<PersonMovement>().leaveActive = false;
                break;
            }
        }

        return target;
    }
}
