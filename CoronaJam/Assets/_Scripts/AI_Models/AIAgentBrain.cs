﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using CRBT;

[RequireComponent(typeof(OpponentFSM))]
public class AIAgentBrain : MonoBehaviour
{
    [SerializeField]
    bool active = false;

    public float disadvantageBrainUpdate = 5f;
    public float advantageBrainUpdate = 5f;
    public float highAdvantageBrainUpdate = 5f;

    private float brainUpdate = 5f;

    OpponentFSM f;

    private void Update()
    {
        if (!GameManager.instance.usingAI || GameManager.instance.paused || active)
            return;
           
        StartAIBrain();
        active = true;
    }

    public void StartAIBrain()
    {
        active = true;

        f = GetComponent<OpponentFSM>();
        f.SetOpponentFSM();
        
        StartCoroutine(IStartBrain());
    }

    private IEnumerator IStartBrain()
    {
        Debug.Log("Brain ON");
        while (!GameManager.instance.ended)
        {
            if (!GameManager.instance.paused)
                f.f.Update();

            if (f.f.current.Equals(FSM_STATES.Acquiring))
                brainUpdate = advantageBrainUpdate;
            else if (f.f.current.Equals(FSM_STATES.Conservative))
                brainUpdate = highAdvantageBrainUpdate;
            else
                brainUpdate = disadvantageBrainUpdate;

            yield return new WaitForSeconds(brainUpdate);
        }
    }
}
