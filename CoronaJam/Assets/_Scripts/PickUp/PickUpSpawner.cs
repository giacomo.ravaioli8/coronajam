﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpSpawner : MonoBehaviour
{
    public MapGrid mapGridP1;
    public MapGrid mapGridP2;

    public GameObject pickUpContainer;

    public GameObject pickUpToSpawn;

    public GameObject arrotinoPrefab;
    public GameEvent _ArrotinoEvent;
    public GameObject ambulancePrefab;
    public GameEvent _AmbulanceEvent;
    public GameObject icecreamPrefab;
    public GameEvent _IceCreamEvent;

    public void SpawnPickUps()
    {
        int choose = Random.Range(0, 100) % 3;

        switch (choose)
        {
            case 0:
                pickUpToSpawn = arrotinoPrefab;
                break;
            case 1:
                pickUpToSpawn = ambulancePrefab;
                break;
            case 2:
                pickUpToSpawn = icecreamPrefab;
                break;
        }

        SpawnPickUp1(choose);
        SpawnPickUp2(choose);
    }

    public void SpawnPickUp1(int choose)
    {
        Node entryP1 = mapGridP1.GetRandomOutNode(null);

        GameObject pickUp1 = Instantiate(pickUpToSpawn, entryP1.transform.position, Quaternion.identity, pickUpContainer.transform);
        pickUp1.GetComponent<PickUp>().SetPickUpType(choose);
        pickUp1.GetComponent<PickUpMovement>().AgentSetup(mapGridP1);
    }

    public void SpawnPickUp2(int choose)
    {
        Node entryP2 = mapGridP2.GetRandomOutNode(null);

        GameObject pickUp2 = Instantiate(pickUpToSpawn, entryP2.transform.position, Quaternion.identity, pickUpContainer.transform);
        pickUp2.GetComponent<PickUp>().SetPickUpType(choose);
        pickUp2.GetComponent<PickUpMovement>().AgentSetup(mapGridP2);
    }
}
