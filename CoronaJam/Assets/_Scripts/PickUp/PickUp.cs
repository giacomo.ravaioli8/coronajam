﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PickUpType { Arrotino, Ambulance, IceCream }

public class PickUp : MonoBehaviour
{
    public PickUpType type;

    [SerializeField]
    private AudioSource emitter;

    public List<AudioClip> pickUpsClips = new List<AudioClip>();

    public void SetPickUpType(int type)
    {
        this.type = (PickUpType) type;
        
        emitter.clip = pickUpsClips[type];
        emitter.Play();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        collision.gameObject.GetComponent<Person>()?.Cure();
    }
}
