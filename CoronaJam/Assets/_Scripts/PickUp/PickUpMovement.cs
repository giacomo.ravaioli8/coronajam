﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpMovement : MovingAgent
{
    public override void AgentSetup(MapGrid g)
    {
        base.AgentSetup(g);

        ExitMap();
        StartCoroutine(IMoveToExit());
    }
}
