﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    [Header("People Spawner")]
    public GameEvent _SpawnPerson;
    [SerializeField]
    private bool stopPeopleSpawn = false;

    [Tooltip("Number of people to spawn")]
    [Range(1, 10)]
    public int numberOfPeopleToSpawn;

    [Tooltip("Frequency (in seconds)")]
    public float spawnFrequency;

    [Tooltip("Frequency variance (in seconds)")]
    public float frequencyVariance;

    public int initialPopulation;
    
    [Header("PickUp Spawner")]
    public GameEvent _SpawnPickUp;
    [SerializeField]
    private bool stopPickUpSpawn = false;

    [Tooltip("Number of pick-ups to spawn")]
    [Range(1, 10)]
    public int numberOfPickUpToSpawn;

    [Tooltip("Frequency (in seconds)")]
    public float pickUpSpawnFrequency;

    [Tooltip("Frequency variance (in seconds)")]
    public float pickUpFrequencyVariance;

    // Start is called before the first frame update
    void Start()
    {
        PopulateMap();

        StartCoroutine(IPeopleSpawner());
        StartCoroutine(IPickUpSpawner());
    }
    
    private void PopulateMap()
    {
        for(int i = 0; i < initialPopulation; i++)
        {
            _SpawnPerson.Raise();
        }
    }

    public void EndGame()
    {
        stopPeopleSpawn = true;
        stopPickUpSpawn = true;
    }

    private IEnumerator IPeopleSpawner()
    {
        float interval = spawnFrequency / numberOfPeopleToSpawn;
        
        while (!stopPeopleSpawn)
        {
            yield return new WaitForSeconds(interval);

            if(!GameManager.instance.paused)
            {
                _SpawnPerson.Raise();

                interval = (spawnFrequency + Random.Range(-frequencyVariance, frequencyVariance)) / numberOfPeopleToSpawn;
            }
        }
    }
    
    private IEnumerator IPickUpSpawner()
    {
        float interval = pickUpSpawnFrequency / numberOfPickUpToSpawn;

        while (!stopPickUpSpawn)
        {
            yield return new WaitForSeconds(interval);

            if (!GameManager.instance.paused)
            {
                _SpawnPickUp.Raise();

                interval = (pickUpSpawnFrequency + Random.Range(-pickUpFrequencyVariance, pickUpFrequencyVariance)) / numberOfPickUpToSpawn;
            }
        }
    }
}
