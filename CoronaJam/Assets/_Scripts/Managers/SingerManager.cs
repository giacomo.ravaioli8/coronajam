﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingerManager : MonoBehaviour
{
    [SerializeField]
    private AudioSource main;

    [SerializeField]
    private AudioSource singer;
    [SerializeField]
    private Animator anim;

    private int singHash;

    // Start is called before the first frame update
    void Start()
    {
        singHash = Animator.StringToHash("Sing");
    }

    public void StartSing()
    {
        StartCoroutine(ISing());
    }

    private IEnumerator ISing()
    {
        singer.Play();
        anim.SetBool(singHash, true);
        Debug.Log("ENTRO");

        yield return new WaitForSeconds(singer.clip.length);

        Debug.Log("EXIT");
        anim.SetBool(singHash, false);
    }
}
