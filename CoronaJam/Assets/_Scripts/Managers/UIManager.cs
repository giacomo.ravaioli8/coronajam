﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    GameSceneTexts sceneTexts;
    string currency;

    [SerializeField]
    private Text influencedP1;
    [SerializeField]
    private Text influencedP2;

    [SerializeField]
    private Text coinsP1;
    [SerializeField]
    private Text coinsP2;

    [SerializeField]
    private Text rateP1;
    [SerializeField]
    private Text rateP2;

    [SerializeField]
    private GameObject contePanel1;
    [SerializeField]
    private GameObject contePanel2;

    [SerializeField]
    private Device smartphone;
    [SerializeField]
    private Text smartphoneCost;
    [SerializeField]
    private Device radio;
    [SerializeField]
    private Text radioCost;
    [SerializeField]
    private Device journal;
    [SerializeField]
    private Text journalCost;
    [SerializeField]
    private Device conte;
    [SerializeField]
    private Text conteCost;

    public GameObject pausePanel;
    public Text pauseBack;

    public GameObject devicesPanel;
    public bool changeTutorial = true;
    public Button tutorialBackButton;

    private void Awake()
    {
        if (!instance)
            instance = this;

        if (instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        sceneTexts = GetComponent<GameSceneTexts>();

        currency = sceneTexts.GetCurrency((int)GameManager.instance.lan.current);

        SetGameSceneLanguage();
    }

    private void SetGameSceneLanguage()
    {
        smartphoneCost.text = smartphone.cost + currency;
        radioCost.text = radio.cost + currency;
        journalCost.text = journal.cost + currency;
        conteCost.text = conte.cost + currency;

        sceneTexts.SetGameSceneTexts();
    }

    public void UpdateNumberOfInfluenced(int playerId)
    {
        if(playerId == 1)
            influencedP1.text = GameManager.instance.player1.GetComponent<PlayerStatus>().numberOfInfluenced.ToString();
        else
            influencedP2.text = GameManager.instance.player2.GetComponent<PlayerStatus>().numberOfInfluenced.ToString();
    }

    public void UpdateCoins(int playerId)
    {
        if (playerId == 1)
            coinsP1.text = GameManager.instance.player1.GetComponent<PlayerStatus>().money + currency;
        else
            coinsP2.text = GameManager.instance.player2.GetComponent<PlayerStatus>().money.ToString() + currency;
    }

    public void UpdateRates(int playerId, int rate)
    {
        if (playerId == 1)
            rateP1.text = "+ " + rate + currency + "/ sec";
        else
            rateP2.text = "+ " + rate + currency + "/ sec";
    }

    public void ShowContePanel(int playerId, bool show)
    {
        if (playerId == 1)
            contePanel2.SetActive(show);
        else
            contePanel1.SetActive(show);
    }

    public void ShowPausePanel(bool show)
    {
        pausePanel.SetActive(show);
    }

    public void ShowTutorialPanel()
    {
        if (changeTutorial)
        {
            changeTutorial = false;
            GameManager.instance.paused = false;
            devicesPanel.SetActive(false);
            tutorialBackButton.interactable = true;
        }
        else
        {
            pausePanel.SetActive(true);
            devicesPanel.SetActive(false);
        }
    }

    public void BackToMenu()
    {
        SceneManager.LoadScene(0);
    }
}
