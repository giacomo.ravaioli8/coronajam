﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Language lan;

    public float gameDuration;

    public bool usingAI = true;
    public VsAI useAI;

    public GameObject player1;
    public GameObject player2;

    public GameObject people1;
    public GameObject people2;

    public int moneyUpdateTime;

    public static GameManager instance = null;

    public SpawnManager spawnManager;
    public UIManager uiManager;
    public AudioManager audioManager;
    
    public bool paused = false;
    public bool ended = false;

    public GameEvent _EndGame;

    // Use this for initialization
    void Awake()
    {
        if (instance == null)
            instance = this;

        if (instance != this)
            Destroy(gameObject);

        usingAI = useAI.useAIAgent;
    }

    // Update is called once per frame
    void Update()
    {
        if (!uiManager.changeTutorial && Input.GetKeyDown(KeyCode.Escape))
            PauseGame(!paused);
    }

    public bool IsConteActive()
    {
        return Conte1InAction() || Conte2InAction();
    }

    public bool Conte1InAction()
    {
        return player1.GetComponent<PlayerStatus>().conteInAction;
    }

    public bool Conte2InAction()
    {
        return player2.GetComponent<PlayerStatus>().conteInAction;
    }

    public void EndGame()
    {
        ended = true;

        _EndGame.Raise();

        spawnManager.EndGame();
        audioManager.PauseAudio(true);
    }

    public void PauseGame(bool pause)
    {
        paused = pause;

        uiManager.ShowPausePanel(pause);
        audioManager.PauseAudio(pause);
    }
}