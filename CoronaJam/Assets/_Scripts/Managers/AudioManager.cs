﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public bool smartphonePlayingFake = true;

    [SerializeField]
    private AudioSource main;

    public AudioClip dropJournal;
    public AudioClip dropSmartphone;
    [SerializeField]
    private AudioSource sounds;
    
    public List<AudioClip> fakeNewsClips = new List<AudioClip>();
    [SerializeField]
    private AudioSource fakeNews;
    
    [SerializeField]
    private AudioSource conte;

    public void PlayJournalSound()
    {
        if (sounds.isPlaying)
            return;

        sounds.clip = dropJournal;
        sounds.Play();
    }

    public void PlaySmartphoneSound()
    {
        if (sounds.isPlaying)
            return;

        sounds.clip = dropSmartphone;
        sounds.Play();

        if(smartphonePlayingFake)
            PlayFakeNews();
    }
    
    public void PlayFakeNews()
    {
        if (fakeNews.isPlaying)
            return;

        int rand = Random.Range(0, fakeNewsClips.Count);
        fakeNews.clip = fakeNewsClips[rand];

        fakeNews.Play();
    }

    public void PlayConteAnnouncement()
    {
        StartCoroutine(IPlayConteClip());
    }

    public void PauseAudio(bool pause)
    {
        if (pause)
        {
            conte.Pause();
            sounds.Pause();
            fakeNews.Pause();
        }
        else
        {
            conte.UnPause();
            sounds.UnPause();
            fakeNews.UnPause();
        }
    }

    public IEnumerator IPlayConteClip()
    {
        sounds.mute = true;
        fakeNews.mute = true;

        conte.Play();

        yield return new WaitForSeconds(conte.clip == null ? 5f : conte.clip.length + 0.5f);

        sounds.mute = false;
        fakeNews.mute = false;
    }
}
