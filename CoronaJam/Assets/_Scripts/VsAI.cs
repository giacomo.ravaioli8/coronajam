﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class VsAI : ScriptableObject
{
    public bool useAIAgent = true;
}
