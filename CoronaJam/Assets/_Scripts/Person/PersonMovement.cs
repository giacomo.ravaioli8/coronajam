﻿using System.Collections;
using UnityEngine;

public class PersonMovement : MovingAgent
{
    [Header("Leaving")]
    public bool leaveActive = true;

    private bool isLeaving = false;
    public float leavingTime = 20f;
    [SerializeField]
    private float leavingTimer = 0f;
    
    // Update is called once per frame
    void Update()
    {
        if (isLeaving || GameManager.instance.paused || GameManager.instance.ended)
            return;

        leavingTimer += Time.deltaTime;

        if (Mathf.Abs((transform.position - nextNode.transform.position).magnitude) <= stopThreshold)
        {
            if (!GameManager.instance.IsConteActive() && leaveActive && leavingTimer >= leavingTime)
            {
                currentNode = nextNode;
                ExitMap();

                StartCoroutine(IMoveToExit());

                isLeaving = true;
            }
            else
            {
                Node n = mapGrid.GetRandomAdjacentNode(nextNode, currentNode);
                currentNode = nextNode;
                nextNode = n;

                Vector3 dir = (nextNode.transform.position - currentNode.transform.position).normalized;
                anim.SetFloat(idX, dir.x);
                anim.SetFloat(idY, dir.y);
            }
        }
        else
        {
            transform.position = Vector3.MoveTowards(transform.position, nextNode.transform.position, Time.deltaTime * speed);
        }
    }

    public override void AgentSetup(MapGrid g)
    {
        base.AgentSetup(g);

        nextNode = mapGrid.GetRandomAdjacentNode(currentNode, null);

        Vector3 dir = (nextNode.transform.position - currentNode.transform.position).normalized;
        anim.SetFloat(idX, dir.x);
        anim.SetFloat(idY, dir.y);
    }
}
