﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonSpawner : MonoBehaviour
{
    public MapGrid mapGridP1;
    public MapGrid mapGridP2;

    public GameObject peopleP1Container;
    public GameObject peopleP2Container;

    public GameObject personPrefab;
    
    public void SpawnPersonP1()
    {
        GameObject person = Instantiate(personPrefab, peopleP1Container.transform);

        person.GetComponent<Person>().SetPlayer(false);
        person.GetComponent<PersonMovement>().AgentSetup(mapGridP1);
    }

    public void SpawnPersonP2()
    {
        GameObject person = Instantiate(personPrefab, peopleP2Container.transform);

        person.GetComponent<Person>().SetPlayer(true);
        person.GetComponent<PersonMovement>().AgentSetup(mapGridP2);
    }
}
