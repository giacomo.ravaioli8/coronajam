﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PersonStatus { Normal, Influenced }

public class Person : MonoBehaviour
{
    //If 0, then player 1 (left). Otherwise player 2 (right).
    [SerializeField]
    private bool player;

    public PersonStatus status = PersonStatus.Normal;

    public Color influencedColorP1;
    public GameEvent _InfluenceEventP1;

    public Color influencedColorP2;
    public GameEvent _InfluenceEventP2;

    public Color normalColor;
    public GameEvent _CureEventP1;
    public GameEvent _CureEventP2;
    
    [SerializeField]
    private bool hasSmartphone = false;
    public GameObject smartphone;
    public Color smartphoneRadiusColorP1;
    public Color smartphoneRadiusColorP2;
    private float smartphoneTimer = 0f;

    private void Update()
    {
        if (hasSmartphone)
        {
            smartphoneTimer += Time.deltaTime;

            if (smartphoneTimer >= smartphone.GetComponent<Smartphone>().device.duration)
            {
                AssignSmartphone(false);
                smartphoneTimer = 0f;
            }   
        }
    }

    public void SetPlayer(bool player)
    {
        this.player = player;
        SetSmartphoneColor(player);
    }

    private void SetSmartphoneColor (bool player)
    {
        if (!player)
        {
            smartphone.GetComponent<SpriteRenderer>().color = smartphoneRadiusColorP1;
        }
        else
        {
            smartphone.GetComponent<SpriteRenderer>().color = smartphoneRadiusColorP2;
        }
    }

    public void AssignSmartphone(bool assignSmartphone)
    {
        hasSmartphone = assignSmartphone;
        smartphone.SetActive(assignSmartphone);

        if (assignSmartphone)
            Influence();
    }

    public void Influence ()
    {
        if (status.Equals(PersonStatus.Influenced))
            return;

        status = PersonStatus.Influenced;

        if (!player)
        {
            _InfluenceEventP1.Raise();
            GetComponent<SpriteRenderer>().color = influencedColorP1;
        }
        else
        {
            _InfluenceEventP2.Raise();
            GetComponent<SpriteRenderer>().color = influencedColorP2;
        }   
    }

    public void Cure ()
    {
        if (status.Equals(PersonStatus.Normal))
            return;

        status = PersonStatus.Normal;
        GetComponent<SpriteRenderer>().color = normalColor;

        if (hasSmartphone)
            AssignSmartphone(false);

        if (!player)
            _CureEventP1.Raise();
        else
            _CureEventP2.Raise();
    }

    public bool IsInfluenced()
    {
        return status.Equals(PersonStatus.Influenced);
    }
}
