﻿using System.Collections.Generic;
using UnityEngine;

public class MapGrid : MonoBehaviour
{
    public Transform nodes;

    public List<Node> outNodes;

    private Graph g;

    private void Awake()
    {
        CreateGraph();
    }

    private void Start()
    {

    }

    private void CreateGraph()
    {
        g = new Graph();

        for (int i = 0; i < nodes.childCount; i++)
        {
            Node n = nodes.GetChild(i).GetComponent<Node>();
            g.AddNode(n);

            foreach (Node connectedN in n.connectedNodes)
            {
                //Debug.Log("Creating edge between " + n.name + " and " + connectedN.name);
                g.AddNode(connectedN);

                g.AddEdge(new Edge(n, connectedN));
                g.AddEdge(new Edge(connectedN, n));
            }
        }
    }

    public Edge[] FindPathToOut(Node currentNode)
    {
        //TODO: Find random exit node
        return AStarSolver.Solve(g, currentNode, GetRandomOutNode(currentNode), EuclideanEstimator);
    }
    
    public Graph GetMapGraph()
    {
        return g;
    }

    public Node GetRandomOutNode(Node currentNode)
    {
        if (currentNode == null)
            return outNodes[Random.Range(0, outNodes.Count)];

        Node n;
        do
        {
            n = outNodes[Random.Range(0, outNodes.Count)];
        } while (currentNode.Equals(n));

        return n;
    }
    
    public Node GetRandomAdjacentNode(Node currentNode, Node previousNode)
    {
        Node adjacent;

        do
        {
            adjacent = currentNode.connectedNodes[Random.Range(0, currentNode.connectedNodes.Count)];
        } while (currentNode.connectedNodes.Count > 1 && adjacent == previousNode);

        return adjacent;
    }

    private float EuclideanEstimator(Node from, Node to)
    {
        return (from.sceneObject.transform.position - to.sceneObject.transform.position).magnitude;
    }
}
